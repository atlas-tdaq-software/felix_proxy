/*
 * ClientThread.h
 *
 *  Created on: Jun 26, 2020
 *      Author: kolos
 */

#ifndef FELIX_PROXY_CLIENTTHREAD_H_
#define FELIX_PROXY_CLIENTTHREAD_H_

#include <memory>

#include <felix/felix_client_thread_extension423.hpp>

#include <ers/ers.h>

/*! \class felix::ImplementationException
 *  This issue is used to report problems with the dynamic library
 *  that implements the FelixClientThread interface
 */
ERS_DECLARE_ISSUE(felix_proxy, LoadLibraryException,
        "FELIX interface implementation library '" << lib_name << "' issue: " << reason,
        ((std::string)lib_name) ((std::string)reason))

/*! \class swrod::NotImplemented
 *  This issue is used to report an attempt to call 
 *  a non implemented function of the FelixClientThread interface
 */
ERS_DECLARE_ISSUE(felix_proxy, FunctionNotImplementedException,
        "'" << function << "' function is not implemented by '" << library
        << "' FelixClient interface implementation",
        ((std::string)function) ((std::string)library))

namespace felix_proxy {

    class ClientThread: public FelixClientThreadExtension423 {

    public:
        explicit ClientThread(const Config &config);

        ~ClientThread();

        void send_data(uint64_t elink_id, const uint8_t *data, size_t size, bool flush) override;

        void subscribe(uint64_t elink_id) override;

        void unsubscribe(uint64_t elink_id) override;

        void exec(const UserFunction &user_function) override;

        /////////////////////////////////////////////////////////////////////
        // Methods added by extension42
        /////////////////////////////////////////////////////////////////////
        void init_send_data(uint64_t fid) override;

        void init_subscribe(uint64_t fid) override;

        Status send_cmd(const std::vector<uint64_t>& fids, Cmd cmd,
                const std::vector<std::string>& cmd_args, std::vector<Reply>& replies) override;

        void subscribe(const std::vector<uint64_t>& fids) override;

        /////////////////////////////////////////////////////////////////////
        // Methods added by extension421
        /////////////////////////////////////////////////////////////////////
        void send_data(uint64_t fid, const std::vector<const uint8_t*>& msgs,
                const std::vector<size_t>& sizes) override;

        /////////////////////////////////////////////////////////////////////
        // Methods added by extension422
        /////////////////////////////////////////////////////////////////////
        void send_data_nb(uint64_t fid, const uint8_t* data, size_t size, bool flush) override;

        void send_data_nb(uint64_t fid, const std::vector<const uint8_t*>& msgs,
                const std::vector<size_t>& sizes) override;

        /////////////////////////////////////////////////////////////////////
        // Methods added by extension423
        /////////////////////////////////////////////////////////////////////
        void user_timer_start(unsigned long interval) override;

        void user_timer_stop() override;

        void callback_on_user_timer(OnUserTimerCallback on_user_timer_cb) override;

    private:
        std::string m_lib_name;
        void * m_lib_handler;
        std::unique_ptr<FelixClientThreadInterface> m_implementation;
        FelixClientThreadExtension42  * m_extension_42  = nullptr;
        FelixClientThreadExtension421 * m_extension_421 = nullptr;
        FelixClientThreadExtension422 * m_extension_422 = nullptr;
        FelixClientThreadExtension423 * m_extension_423 = nullptr;
    };
}

#endif /* FELIX_PROXY_CLIENTTHREAD_H_ */
