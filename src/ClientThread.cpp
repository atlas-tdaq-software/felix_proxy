/*
 * ClientThread.cpp
 *
 *  Created on: Jun 26, 2020
 *      Author: kolos
 */

#include <dlfcn.h>
#include <link.h>

#include <ers/ers.h>

#include <felix_proxy/ClientThread.h>

using namespace felix_proxy;

namespace {
    const std::string c_library_name("libfelix-client-lib.so");
    const std::string c_factory_name("create_felix_client");
}

ClientThread::ClientThread(const Config &config) {
    // load shared library that implements the FelixClientThreadInterface
    // from FELIX releaseAPI from the FELIX release
    m_lib_handler = dlopen(c_library_name.c_str(), RTLD_NOW);
    if (!m_lib_handler) {
        throw felix_proxy::LoadLibraryException(ERS_HERE, c_library_name, dlerror());
    }

    // get a pointer to the function that can create a new instance
    // of the FelixClientThreadInterface implementation
    void *sym = dlsym(m_lib_handler, c_factory_name.c_str());
    if (!sym) {
        throw felix_proxy::LoadLibraryException(ERS_HERE, c_library_name, dlerror());
    }

    typedef FelixClientThreadInterface* (*ImplFactory)(const Config &config);

    ImplFactory factory = reinterpret_cast<decltype(factory)>(sym);

    m_implementation.reset(factory(config));

    m_extension_42  = dynamic_cast<FelixClientThreadExtension42*> (m_implementation.get());
    m_extension_421 = dynamic_cast<FelixClientThreadExtension421*>(m_implementation.get());
    m_extension_422 = dynamic_cast<FelixClientThreadExtension422*>(m_implementation.get());
    m_extension_423 = dynamic_cast<FelixClientThreadExtension423*>(m_implementation.get());

    link_map* info;
    if (dlinfo(m_lib_handler, RTLD_DI_LINKMAP, &info) != -1) {
        m_lib_name = info->l_name;
    }

    ERS_LOG("FelixClient implementation (extended = " << !!m_extension_42 << ") has been loaded from "
            << m_lib_name << " library");
}

ClientThread::~ClientThread() {
    m_implementation.reset();
    dlclose(m_lib_handler);
}

void ClientThread::send_data(uint64_t elink_id, const uint8_t *data, size_t size, bool flush) {
    m_implementation->send_data(elink_id, data, size, flush);
}

void ClientThread::subscribe(uint64_t elink_id) {
    m_implementation->subscribe(elink_id);
}

void ClientThread::unsubscribe(uint64_t elink_id) {
    m_implementation->unsubscribe(elink_id);
}

void ClientThread::exec(const UserFunction &user_function) {
    m_implementation->exec(user_function);
}

/////////////////////////////////////////////////////////////////////
// Methods added by extension42
/////////////////////////////////////////////////////////////////////
void ClientThread::init_send_data(uint64_t fid) {
    if (not m_extension_42) {
        throw FunctionNotImplementedException(ERS_HERE, __FUNCTION__, m_lib_name);
    }

    m_extension_42->init_send_data(fid);
}

void ClientThread::init_subscribe(uint64_t fid) {
    if (not m_extension_42) {
        throw FunctionNotImplementedException(ERS_HERE, __FUNCTION__, m_lib_name);
    }

    m_extension_42->init_subscribe(fid);
}

ClientThread::Status ClientThread::send_cmd(const std::vector<uint64_t>& fids, Cmd cmd,
        const std::vector<std::string>& cmd_args, std::vector<Reply>& replies) {
    if (not m_extension_42) {
        throw FunctionNotImplementedException(ERS_HERE, __FUNCTION__, m_lib_name);
    }

    return m_extension_42->send_cmd(fids, cmd, cmd_args, replies);
}

void ClientThread::subscribe(const std::vector<uint64_t>& fids) {
    if (not m_extension_42) {
        throw FunctionNotImplementedException(ERS_HERE, __FUNCTION__, m_lib_name);
    }

    m_extension_42->subscribe(fids);
}

/////////////////////////////////////////////////////////////////////
// Methods added by extension421
/////////////////////////////////////////////////////////////////////
void ClientThread::send_data(uint64_t fid, const std::vector<const uint8_t*>& msgs,
        const std::vector<size_t>& sizes) {
    if (not m_extension_421) {
        throw FunctionNotImplementedException(ERS_HERE, __FUNCTION__, m_lib_name);
    }

    m_extension_421->send_data(fid, msgs, sizes);
}

/////////////////////////////////////////////////////////////////////
// Methods added by extension422
/////////////////////////////////////////////////////////////////////
void ClientThread::send_data_nb(uint64_t fid, const uint8_t* data, size_t size, bool flush) {
    if (not m_extension_422) {
        throw FunctionNotImplementedException(ERS_HERE, __FUNCTION__, m_lib_name);
    }

    m_extension_422->send_data_nb(fid, data, size, flush);
}

void ClientThread::send_data_nb(uint64_t fid, const std::vector<const uint8_t*>& msgs,
        const std::vector<size_t>& sizes) {
    if (not m_extension_422) {
        throw FunctionNotImplementedException(ERS_HERE, __FUNCTION__, m_lib_name);
    }

    m_extension_422->send_data_nb(fid, msgs, sizes);
}

/////////////////////////////////////////////////////////////////////
// Methods added by extension423
/////////////////////////////////////////////////////////////////////
void ClientThread::user_timer_start(unsigned long interval) {
    if (not m_extension_423) {
        throw FunctionNotImplementedException(ERS_HERE, __FUNCTION__, m_lib_name);
    }

    m_extension_423->user_timer_start(interval);
}

void ClientThread::user_timer_stop() {
    if (not m_extension_423) {
        throw FunctionNotImplementedException(ERS_HERE, __FUNCTION__, m_lib_name);
    }

    m_extension_423->user_timer_stop();
}

void ClientThread::callback_on_user_timer(OnUserTimerCallback on_user_timer_cb) {
    if (not m_extension_423) {
        throw FunctionNotImplementedException(ERS_HERE, __FUNCTION__, m_lib_name);
    }

    m_extension_423->callback_on_user_timer(on_user_timer_cb);
}
